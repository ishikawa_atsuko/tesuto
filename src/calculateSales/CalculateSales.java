 package calculateSales;

 import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

 public class CalculateSales {
	 public static void main(String[] args){

//		 System.out.println("ここにあるファイルを開きます = >" + args[0]);
		 HashMap<String,String> map1 = new HashMap<String,String>();
		 HashMap<String,Integer> map2 = new HashMap<String,Integer>();

		 BufferedReader br = null;
		 try {

			//branch.lstを取り込み
			 File file = new File(args[0], "branch.lst");
			 FileReader fr = new FileReader(file);
			 br = new BufferedReader(fr);

			 //branch.lstを一行ずつ取り込み
			 String line;
			 while ((line = br.readLine()) != null) {
				 String str = line;
				  Pattern p2 = Pattern.compile(".*[,].*");
				  Matcher m2 = p2.matcher(str);

				  if( m2.find() == false){
					  System.out.println("支店定義ファイルのフォーマットが不正です。");
					  System.exit(0);

				  } else {

				 String[] name = str.split(",");


				 Pattern p = Pattern.compile("^\\d{3}$");

				 Pattern p3 = Pattern.compile(".*\\r\\n.*");
				 Pattern p4 = Pattern.compile("^[0-9]*$");
				 Matcher m = p.matcher(name[0]);
				 Matcher m3 = p3.matcher(name[1]);
				 Matcher m4 = p4.matcher(name[1]);

				  if ( m.find() == false || m3.find() == true || m4.find() == true){
					 System.out.println("支店定義ファイルのフォーマットが不正です。");
					 System.exit(0);

				  }else{

				//map1に支店コードと支店名をput
				 map1.put(name[0],name[1]);

				//map2に支店コードと支店合計額（0円）をput
				 map2.put(name[0],0);

				  }
				 }

			 //ファイルがなかった時のエラー処理
			 }
			 } catch (FileNotFoundException e ) {
				 System.out.println("支店定義ファイルが存在しません。");
				 System.exit(0);

			//エラーが出たとき
		 } catch (IOException e) {
			 System.out.println("エラーが発生しました。");
		   } finally {
			 if (br != null) {
				 try {
					 br.close();
				 } catch (IOException e) {
					 System.out.println("closeできませんでした。");
				   }
			 }
		     }

//		 for (Entry<String, String> entry : map.entrySet()) {
//			 System.out.println(entry.getKey() + ":" + entry.getValue());   //支店コードと支店名を別々で表示
//		 }
		 System.out.println("   ");

		 BufferedReader bu = null;
         try {
        	 File filee = new File(args[0]);

        	 //売上ファイル一覧を出す
        	 File files[] = filee.listFiles();

		 for (int i = 0; i < files.length; i++){
			 String st = files[i].getName();

			//8桁の数字の名前　かつ　"rcd"ファイル名を検索
			 if (st.endsWith("rcd") && st.matches(".*[0-9]{8}.*")){

//				 for (int in =0; in<st.length(); in++){
//				 if ( st.matches(String.format("^%08d",in++ )) == false){
//					 System.out.println("売上ファイル名が連番になっていません。");
//					 System.exit(0);
//				 }
//				 }
//			 }else {

				 File fileee = new File(args[0],st);
				 FileReader fre = new FileReader(fileee);
				 bu = new BufferedReader(fre);
				 String lines = bu.readLine();
				 Integer lines2 = Integer.parseInt(bu.readLine());


				//map2に支店コードと売上金額をput(支店合計額と売上金額が足される)
				 map2.put(lines,lines2);


		 PrintWriter pw = new PrintWriter ( new BufferedWriter ( new FileWriter ("C:\\Users\\admin\\Desktop\\売り上げ集計課題\\branch.out")));
		 for (Entry<String, String> entry : map1.entrySet()) {
			 pw.write(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey())+"\r\n");
//			 System.out.println(entry.getKey() + "," + entry.getValue() + "," + map2.get(entry.getKey()));
		 }
		 pw.close();
         }
         }
         }
			   catch (IOException e){
				 System.out.println("エラーが発生しました。");
			   } finally {
				   if (bu != null) {
					 try {
						 bu.close();
					 } catch (IOException e) {
							 System.out.println("closeできませんでした。");
					   } finally {
					     }
			        }
			     }
	 }
 }
